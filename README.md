Hi ![](https://user-images.githubusercontent.com/18350557/176309783-0785949b-9127-417c-8b55-ab5a4333674e.gif)My name is Ashutosh Aswal
======================================================================================================================================

Talks about Android, Kotlin and anime. 
-----------------

⚡ I am a developer with a niche in android development. I do have quite an interest in learning and exploring different technologies.

* 🌍  I'm based in Chandigarh,India
* 🖥️  See my portfolio at [yellowhatpro.vercel.app](http://yellowhatpro.vercel.app)
* ✉️  You can contact me at [yellowhatpro3119@gmail.com](mailto:yellowhatpro3119@gmail.com)
* 🌲  Visit my Linktree at [linktr.ee/yellowhatpro](https://linktr.ee/yellowhatpro)
* 🚀  I'm currently focusing on Competitive Programming 🚀
* 🧠  I'm learning Data Structures and Algorithms 
* 🤝  I'm open to collaborating on Android Projects

### Skills
<p align="left">
<a href="https://developer.android.com" target="_blank" rel="noreferrer"> <img src="https://user-images.githubusercontent.com/67560900/135036263-b84d2aae-75e0-41d4-a38e-7bd47ca684a8.png" alt="android" width="60" height="60"/> </a>
<a href="https://kotlinlang.org/" target="_blank" rel="noreferrer"><img src="https://user-images.githubusercontent.com/67560900/107707894-9d2b3880-6ce8-11eb-8dda-9f7332696242.png" width="60" height="60" alt="Kotlin" /></a>
<a href="https://docs.microsoft.com/en-us/cpp/?view=msvc-170" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/cplusplus-colored.svg" width="60" height="60" alt="C++" /></a>
<a href="https://www.oracle.com/java/" target="_blank" rel="noreferrer"><img src="https://user-images.githubusercontent.com/67560900/107707714-53425280-6ce8-11eb-81e8-d0c3e2eb51f2.png" width="60" height="60" alt="Java" /></a>
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/javascript-colored.svg" width="60" height="60" alt="JavaScript" /></a>
<a href="https://www.python.org/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/python-colored.svg" width="60" height="60" alt="Python" /></a>
<a href="https://go.dev/doc/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/go-colored.svg" width="60" height="60" alt="Go" /></a>
<a href="https://reactjs.org/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/react-colored.svg" width="60" height="60" alt="React" /></a>
<a href="https://developer.mozilla.org/en-US/docs/Glossary/HTML5" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/html5-colored.svg" width="60" height="60" alt="HTML5" /></a>
<a href="https://www.w3.org/TR/CSS/#css" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/css3-colored.svg" width="60" height="60" alt="CSS3" /></a>
<a href="https://sass-lang.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/sass-colored.svg" width="60" height="60" alt="Sass" /></a>
<a href="https://getbootstrap.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/bootstrap-colored.svg" width="60" height="60" alt="Bootstrap" /></a>
<a href="https://nodejs.org/en/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/nodejs-colored.svg" width="60" height="60" alt="NodeJS" /></a>
<a href="https://www.mongodb.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/mongodb-colored.svg" width="60" height="60" alt="MongoDB" /></a>
  <a href="https://spring.io/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/springio/springio-icon.svg" alt="spring" width="60" height="60"/> </a> <a href="https://www.sqlite.org/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/sqlite/sqlite-icon.svg" alt="sqlite" width="60" height="60"/> </a>
<a href="https://firebase.google.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/firebase-colored.svg" width="60" height="60" alt="Firebase" /></a>
<a href="https://www.mysql.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/mysql-colored.svg" width="60" height="60" alt="MySQL" /></a>
<a href="https://www.heroku.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/heroku-colored.svg" width="60" height="60" alt="Heroku" /></a>
<a href="https://www.figma.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/figma-colored.svg" width="60" height="60" alt="Figma" /></a> <a href="https://www.arduino.cc/" target="_blank" rel="noreferrer">
 <a href="https://gradle.org"><img src="https://www.vectorlogo.zone/logos/gradle/gradle-icon.svg" width="66" height="66" alt ="gradle"></a>
 <img src="https://cdn.worldvectorlogo.com/logos/arduino-1.svg" alt="arduino" width="60" height="60"/> </a><a href="https://postman.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="60" height="60"/> </a><a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/ShahriarShafin/ShahriarShafin/main/Assets/git.gif" alt="git" width="110" height="60"/> </a>
</p>

### Socials

<p align="left"> <a href="https://www.dev.to/yellowhatpro" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/devto.svg" width="60" height="60" /></a>
  <a href="https://discord.com/users/787332879054536704" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/discord.svg" width="60" height="60" /></a>
  <a href="https://www.github.com/yellowhatpro" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/github-dark.svg" width="60" height="60" /></a>
  <a href="http://www.instagram.com/_aashu_aswal_" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/instagram.svg" width="60" height="60" /></a> 
  <a href="https://www.linkedin.com/in/ashutoshaswal" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/linkedin.svg" width="60" height="60" /></a>
  <a href="https://www.polywork.com/yellowhatpro" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/polywork.svg" width="60" height="60" /></a> 
  <a href="https://www.twitter.com/yellowhatpro" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/twitter.svg" width="60" height="60" /></a>
  <a href="https://stackoverflow.com/users/15084244" target="blank"><img src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/stack-overflow.svg" alt="15084244" height="60" width="60" /></a>
  <a href="https://kaggle.com/yellowhatpro" target="blank"><img  src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/kaggle.svg" alt="yellowhatpro" height="60" width="60" /></a>
<a href="https://www.hackerrank.com/yellowhatpro" target="blank"><img  src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/hackerrank.svg" alt="yellowhatpro" height="60" width="60" /></a> 
<a href="https://codeforces.com/profile/yellowhatpro" target="blank"><img  src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/codeforces.svg" alt="yellowhatpro" height="60" width="60" /></a>
<a href="https://www.leetcode.com/yellowhatpro" target="blank"><img src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/leet-code.svg" alt="yellowhatpro" height="60" width="60" /></a>
  <a href="https://auth.geeksforgeeks.org/user/yellowhatpro/profile" target="blank"><img src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/geeks-for-geeks.svg" alt="yellowhatpro/profile" height="60" width="60" /></a>
  <a href="https://medium.com/@yellowhatpro" target="blank"><img src="https://cdn.worldvectorlogo.com/logos/medium-m-2.svg" alt="@yellowhatpro" height="60" width="60" /></a>  
</p>

### Stats

<a href="https://wakatime.com/@4b58986d-0cb8-41bd-bd6b-69c8a30d0a57"><img src="https://wakatime.com/badge/user/4b58986d-0cb8-41bd-bd6b-69c8a30d0a57.svg" alt="Total time coded since Sep 9 2022" /></a>

<p >&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api/wakatime?username=yellowhatpro&layout=compact&theme=tokyonight" /></p>
